# Introduction

App is to fetch and display list of schools in NY and show the details if user clicks on any school from list 
It Uses 
1. Rx Library for asynchronous calls
2. Retrofit Library for Networking 
3.Uses Hilt for DI
3. LiveData / Lifecycle and VM from jetpack

# App architecture
App is multi-module project and follows MVVM architecture.
List of Modules
1. App - Holds UI  / VM components
2. core(base) - Holds Base classes for Fragments / VM  
3. Data - Holds classes and logic to handle data layer (fetch data from network / save it / process it)
4. Model - Holds Data classes required by both network and data layer
5. Network- Holds core logic to generate network client also holds classes/interfaces with endpoint information

# What could have been done better
1. Integration of Paging to show data directly from DB
2. Result class (in repo) could have been simply sealed class.
3. Could have added test cases
4. More comments
5. Extracting constant for dependency versions in build.gradle file
6. UI could have been more cleaner