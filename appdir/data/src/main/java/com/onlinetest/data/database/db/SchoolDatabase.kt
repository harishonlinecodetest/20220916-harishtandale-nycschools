package com.onlinetest.data.database.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.onlinetest.data.database.dao.SchoolDao
import com.onlinetest.model.domain.School

@Database(entities = [School::class], exportSchema = false, version = 1)
abstract class SchoolDatabase : RoomDatabase() {
    abstract fun schoolDao() : SchoolDao
}