package com.onlinetest.data.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.onlinetest.model.domain.School
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface SchoolDao {

    @Query("SELECT * FROM School")
    fun getAllSchools(): Single<List<School>>

    @Query("SELECT * FROM School where dbn =:schoolId")
    fun getSchool(schoolId: String): Single<School>

    @Insert
    fun insertSchool(school: School)

    @Query("DELETE FROM School")
    fun deleteAllSchools()

}