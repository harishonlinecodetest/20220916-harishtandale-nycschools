package com.onlinetest.data.repository.impl

import com.onlinetest.data.database.dao.SchoolDao
import com.onlinetest.data.repository.SchoolAPIRepository
import com.onlinetest.model.SchoolResponse
import com.onlinetest.model.SchoolSatScore
import com.onlinetest.model.domain.School
import com.onlinetest.network.Result
import com.onlinetest.network.SchoolApiService
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import javax.inject.Inject

const val UNABLE_FETCH_DATA_MSG = "Unable to fetch data. \n Please try again"
const val NETWORK_DATA_MSG = "Unable to fetch data. \n Please check network and try again"

/**
 * Implementation class of SchoolAPIRepository
 */
class SchoolAPIRepositoryImpl @Inject constructor(
    private val schoolDao: SchoolDao,
    private val schoolApiService: SchoolApiService
) : SchoolAPIRepository {
    override fun fetchSchoolData(): Single<List<School>> {
        return schoolDao.getAllSchools()
            .flatMap {
                if (it.isEmpty()) {
                    fetchSchoolDetails()
                } else {
                    Single.just(it)
                }
            }.subscribeOn(Schedulers.io())
            .onErrorReturn {
                emptyList()
            }
    }

    override fun fetchAndInsertSchoolData(): Single<Result> {
        return schoolDao.getAllSchools()
            .flatMap {
                if (it.isEmpty()) {
                    fetchSchoolDetails()
                } else {
                    Single.just(it)
                }
            }.flatMap {
                if (it.isEmpty()) {
                    Single.just(Result(NETWORK_DATA_MSG))
                } else {
                    Single.just(Result())
                }
            }.subscribeOn(Schedulers.io())
            .onErrorReturn { throwable ->
                getResult(throwable)
            }
    }

    private fun getResult(throwable: Throwable): Result {
        return if (throwable is HttpException) {
            Result(NETWORK_DATA_MSG)
        } else {
            Result(UNABLE_FETCH_DATA_MSG)
        }

    }

    override fun getSchool(schoolId: String): Single<School> {
        return schoolDao.getSchool(schoolId)
    }

    private fun fetchSchoolDetails(): Single<List<School>> {
        return Single.zip(
            schoolApiService.fetchSchools(),
            schoolApiService.fetchSchoolDetails()
        ) { schoolList: List<SchoolResponse>, schoolDetailList: List<SchoolSatScore> ->
            mergeListsAndInsertInDb(schoolList, schoolDetailList)
        }.onErrorReturn {
            emptyList()
        }
    }

    // NOTE: - Logic for the following function is extremely slow
    //because of find call , it could have been avoided if two separate tables used
    //and dump the response in its own table later get the value of merged object when details screen loaded

    private fun mergeListsAndInsertInDb(
        schoolList: List<SchoolResponse>,
        schoolDetailList: List<SchoolSatScore>
    ): List<School> {
        if (schoolList.isEmpty() || schoolDetailList.isEmpty()) {
            return emptyList()
        }
        val resultList = mutableListOf<School>()
        schoolList.forEach { element ->
            val detailElement = schoolDetailList.find { it.dbn == element.dbn }
            val school = School(
                dbn = element.dbn,
                school_name = element.school_name,
                boro = element.boro,
                overview_paragraph = element.overview_paragraph,
                school_10th_seats = element.school_10th_seats,
                academicopportunities1 = element.academicopportunities1,
                academicopportunities2 = element.academicopportunities2,
                phone_number = element.phone_number,
                fax_number = element.fax_number,
                school_email = element.school_email,
                website = element.website,
                subway = element.subway,
                bus = element.bus,
                finalgrades = element.finalgrades,
                total_students = element.total_students,
                extracurricular_activities = element.extracurricular_activities,
                school_sports = element.school_sports,
                school_accessibility_description = element.school_accessibility_description,
                requirement1_1 = element.requirement1_1,
                requirement2_1 = element.requirement2_1,
                requirement3_1 = element.requirement3_1,
                requirement4_1 = element.requirement4_1,
                requirement5_1 = element.requirement5_1,
                program1 = element.program1,
                code1 = element.code1,
                interest1 = element.interest1,
                grade9gefilledflag1 = element.grade9gefilledflag1,
                grade9geapplicants1 = element.grade9geapplicants1,
                seats9swd1 = element.seats9swd1,
                grade9swdfilledflag1 = element.seats9ge1,
                grade9swdapplicants1 = element.grade9swdapplicantsperseat1,
                seats101 = element.seats101,
                admissionspriority11 = element.admissionspriority11,
                admissionspriority21 = element.admissionspriority21,
                admissionspriority31 = element.admissionspriority31,
                grade9geapplicantsperseat1 = element.grade9geapplicantsperseat1,
                grade9swdapplicantsperseat1 = element.grade9swdapplicantsperseat1,
                primary_address_line_1 = element.primary_address_line_1,
                city = element.city,
                zip = element.zip,
                state_code = element.state_code,
                latitude = element.latitude,
                longitude = element.longitude,
                num_of_sat_test_takers = detailElement?.num_of_sat_test_takers ?: "",
                sat_critical_reading_avg_score = detailElement?.sat_critical_reading_avg_score
                    ?: "",
                sat_math_avg_score = detailElement?.sat_math_avg_score ?: "",
                sat_writing_avg_score = detailElement?.sat_writing_avg_score ?: ""
            )
            schoolDao.insertSchool(school)
            resultList.add(school)
        }
        return resultList
    }
}