package com.onlinetest.data.di

import android.content.Context
import androidx.room.Room
import com.onlinetest.data.database.dao.SchoolDao
import com.onlinetest.data.database.db.SchoolDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {
    @Provides
    fun provideSchoolDao(db: SchoolDatabase): SchoolDao {
        return db.schoolDao()
    }

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): SchoolDatabase {
        return Room.databaseBuilder(
            appContext,
            SchoolDatabase::class.java,
            "SchoolDB"
        ).build()
    }
}

