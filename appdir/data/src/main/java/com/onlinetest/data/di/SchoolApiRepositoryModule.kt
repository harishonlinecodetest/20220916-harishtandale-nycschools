package com.onlinetest.data.di

import com.onlinetest.data.repository.SchoolAPIRepository
import com.onlinetest.data.repository.impl.SchoolAPIRepositoryImpl
import com.onlinetest.network.SchoolApiService
import com.onlinetest.network.di.NetworkClientModule
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module(includes = [SchoolApiServiceModule::class, DatabaseModule::class])
@InstallIn(SingletonComponent::class)
abstract class SchoolApiRepositoryModule {

    @Binds
    abstract fun provideSchoolRepository(schoolAPIRepositoryImpl: SchoolAPIRepositoryImpl): SchoolAPIRepository
}

@Module(includes = [NetworkClientModule::class])
@InstallIn(SingletonComponent::class)
class SchoolApiServiceModule {

    @Provides
    fun provideSchoolApiService(retrofit: Retrofit): SchoolApiService {
        return retrofit.create(SchoolApiService::class.java)
    }
}
