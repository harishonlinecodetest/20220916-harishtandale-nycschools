package com.onlinetest.data.repository

import com.onlinetest.model.domain.School
import io.reactivex.Single

/**
 * interface responsible to fetch data from either DB or Network and provide to UI
 * @see SchoolAPIRepositoryImpl file for implementation
 */
interface
SchoolAPIRepository{
    fun fetchSchoolData(): Single<List<School>>
    fun fetchAndInsertSchoolData(): Single<com.onlinetest.network.Result>
    fun getSchool(schoolId : String): Single<School>
}