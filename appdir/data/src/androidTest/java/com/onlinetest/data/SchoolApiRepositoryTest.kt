package com.onlinetest.data

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.onlinetest.data.database.dao.SchoolDao
import com.onlinetest.data.database.db.SchoolDatabase
import com.onlinetest.data.repository.SchoolAPIRepository
import com.onlinetest.data.repository.impl.SchoolAPIRepositoryImpl
import com.onlinetest.network.SchoolApiService
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class SchoolApiRepositoryTest {

    private lateinit var schoolAPIRepository: SchoolAPIRepository
    private lateinit var schoolDatabase: SchoolDatabase

    @RelaxedMockK
    lateinit var schoolDao: SchoolDao

    @RelaxedMockK
    lateinit var schoolApiService: SchoolApiService

    @Before
    fun setup(){
        MockKAnnotations.init(relaxed = true)
        schoolAPIRepository = SchoolAPIRepositoryImpl(
            schoolDao,schoolApiService
        )

        val context = ApplicationProvider.getApplicationContext<Context>()
        schoolDatabase = Room.inMemoryDatabaseBuilder(
            context, SchoolDatabase::class.java).build()
        schoolDao = schoolDatabase.schoolDao()
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        schoolDatabase.close()
    }

    @Test
    fun readResponseFromNetwork(){
        //GIVEN
        every { schoolApiService.fetchSchools() } returns Single.just(emptyList())
        val result = schoolAPIRepository.fetchSchoolData().blockingGet()
        assert(result.isEmpty())
    }

}