package com.onlinetest.network.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.time.Duration
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val CONNECT_TIMEOUT = 60L
private const val READ_TIMEOUT = 60L
private const val API_BASE_URL = "https://data.cityofnewyork.us/"

@Module(includes = [OkHttpClientModule::class])
@InstallIn(SingletonComponent::class)
class NetworkClientModule {

    @Provides
    @Singleton
    fun provideNetworkClient(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }
}

@Module
@InstallIn(SingletonComponent::class)
class OkHttpClientModule {

    @Provides
    fun  provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(Duration.ofSeconds(READ_TIMEOUT))
            .connectTimeout(Duration.ofSeconds(CONNECT_TIMEOUT))
            .build()

    }
}