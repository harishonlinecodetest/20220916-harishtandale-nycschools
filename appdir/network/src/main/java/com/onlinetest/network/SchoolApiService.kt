package com.onlinetest.network

import com.onlinetest.model.SchoolResponse
import com.onlinetest.model.SchoolSatScore
import io.reactivex.Single
import retrofit2.http.GET

interface SchoolApiService {

    @GET("resource/s3k6-pzi2.json")
    fun fetchSchools():Single<List<SchoolResponse>>

    @GET("resource/f9bf-2cp4.json")
    fun fetchSchoolDetails(): Single<List<SchoolSatScore>>

}