package com.onlinetest.schoolapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout.VERTICAL
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.Orientation
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.onlinetest.core.BaseFragment
import com.onlinetest.schoolapp.R
import com.onlinetest.schoolapp.SchoolListAdapter
import com.onlinetest.schoolapp.databinding.FragmentSchoolListBinding
import com.onlinetest.schoolapp.vm.SchoolListViewModel

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */

class SchoolListFragment : BaseFragment() {

    private var _binding: FragmentSchoolListBinding? = null

    private val schoolListViewModel by viewModels<SchoolListViewModel>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSchoolListBinding.inflate(inflater, container, false)
        initialize()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        schoolListViewModel.eventStream.onNext(SchoolListViewModel.LoadDataEvent)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

     private fun initialize() {
         val dividerItemDecoration = DividerItemDecoration(
             context,
             DividerItemDecoration.VERTICAL
         )
         _binding?.recyclerView?.addItemDecoration(dividerItemDecoration)
        schoolListViewModel.mutableSchoolList.observe(viewLifecycleOwner
        ) { data ->
            with(_binding){
                this?.recyclerView ?.adapter = SchoolListAdapter(data) { schoolId
                    ->
                    findNavController().navigate(
                        R.id.action_FirstFragment_to_SecondFragment,
                        bundleOf("schoolId" to schoolId)
                    )
                }
            }
        }
        schoolListViewModel.showProgress.observe(viewLifecycleOwner) { showProgress ->
            with(_binding){
                this?.group?.visibility = if (showProgress) View.VISIBLE else View.GONE
                this?.recyclerView?.visibility = if (showProgress) View.GONE else View.VISIBLE
            }
        }
    }
}