package com.onlinetest.schoolapp.vm

import androidx.lifecycle.MutableLiveData
import com.onlinetest.core.BaseViewModel
import com.onlinetest.data.repository.SchoolAPIRepository
import com.onlinetest.model.domain.School
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor(
    //HACK - With given time constraints have to add the repo directly otherwise
    // A use-case should be created and passed to specific VM
    private val schoolRepository: SchoolAPIRepository
) : BaseViewModel() {
    val eventStream = PublishSubject.create<InnerEvent>()
    val mutableSchoolList = MutableLiveData<List<School>>(listOf())
    val showProgress = MutableLiveData(false)

    init {
        fetchSchoolData()
    }

    private fun fetchSchoolData() {
        if (mutableSchoolList.value?.isEmpty() == true) {
            showProgress.postValue(true)
            compositeDisposable.add(
                schoolRepository.fetchSchoolData()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { schoolList ->
                        showProgress.postValue(false)
                        handleResponse(schoolList)
                    }
            )
        }
    }

    private fun handleResponse(schoolList: List<School>) {
        if (schoolList.isEmpty()) {
            //show UI to retry
        } else {
            //update UI
            mutableSchoolList.postValue(schoolList)
        }
    }

    sealed class Event()
    sealed class InnerEvent : Event()
    object LoadDataEvent : InnerEvent()
}