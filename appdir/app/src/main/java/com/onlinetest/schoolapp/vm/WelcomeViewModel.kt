package com.onlinetest.schoolapp.vm

import androidx.lifecycle.MutableLiveData
import com.onlinetest.core.BaseViewModel
import com.onlinetest.data.repository.SchoolAPIRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@HiltViewModel
class WelcomeViewModel @Inject constructor(
    //HACK - With given time constraints have to add the repo directly otherwise
    // A use-case should be created and passed to specific VM
    private val schoolRepository: SchoolAPIRepository
) : BaseViewModel() {

    val showProgress = MutableLiveData(false)
    val eventStream = PublishSubject.create<OuterEvent>()

    internal fun fetchAndInsertSchoolData() {
        showProgress.postValue(true)
        compositeDisposable.add(
            schoolRepository.fetchAndInsertSchoolData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result->
                    showProgress.postValue(false)
                    if(result.errorMsg.isEmpty()) {
                        eventStream.onNext(LoadDataEventSuccessEvent)
                    }else {
                        eventStream.onNext(LoadDataEventErrorEvent(result.errorMsg))
                    }
                }
        )
    }

    sealed class OuterEvent
    object LoadDataEventSuccessEvent : OuterEvent()
    class LoadDataEventErrorEvent(val errorMessage:String) : OuterEvent()
}