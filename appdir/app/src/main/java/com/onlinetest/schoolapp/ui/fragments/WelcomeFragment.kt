package com.onlinetest.schoolapp.ui.fragments

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.onlinetest.core.BaseFragment
import com.onlinetest.schoolapp.R
import com.onlinetest.schoolapp.databinding.FragmentSchoolDetailsBinding
import com.onlinetest.schoolapp.databinding.FragmentWelcomeBinding
import com.onlinetest.schoolapp.ui.hideActionBar
import com.onlinetest.schoolapp.ui.showActionBar
import com.onlinetest.schoolapp.vm.WelcomeViewModel
import io.reactivex.subjects.PublishSubject

/**
 * Welcome screen to show a welcome message and fetch and insert dat in background
 */
class WelcomeFragment : BaseFragment() {

    private var _binding: FragmentWelcomeBinding? = null
    private val viewModel by viewModels<WelcomeViewModel>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentWelcomeBinding.inflate(inflater, container, false)
        viewModel.showProgress.observe(viewLifecycleOwner) {
            if (it)
                _binding?.progressBar?.visibility = View.VISIBLE
            else
                _binding?.progressBar?.visibility = View.GONE
        }
        initialize()
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        hideActionBar()
    }

    override fun onStop() {
        showActionBar()
        super.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initialize() {
        _binding?.btnRetry?.setOnClickListener {
            viewModel.fetchAndInsertSchoolData()
            it.visibility = View.GONE
            _binding?.txtState?.text = getString(R.string.load_data_text)
        }
        compositeDisposable.add(viewModel.eventStream.subscribe {
            when (it) {
                is WelcomeViewModel.LoadDataEventSuccessEvent -> {
                    findNavController().navigate(R.id.action_welcomeFragment_to_FirstFragment)
                }
                is WelcomeViewModel.LoadDataEventErrorEvent -> {
                    _binding?.txtState?.text = it.errorMessage
                    _binding?.btnRetry?.visibility=View.VISIBLE
                }
                else -> {}
            }
        })
        viewModel.fetchAndInsertSchoolData()
    }
}