package com.onlinetest.schoolapp.ui.fragments

import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.onlinetest.core.BaseFragment
import com.onlinetest.schoolapp.R
import com.onlinetest.schoolapp.databinding.FragmentSchoolDetailsBinding
import com.onlinetest.schoolapp.vm.SchoolDetailViewModel

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SchoolDetailFragment() : BaseFragment() {

    private var _binding: FragmentSchoolDetailsBinding? = null

    private val viewModel by viewModels<SchoolDetailViewModel>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSchoolDetailsBinding.inflate(inflater, container, false)
        initialize()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.fetchData(arguments?.getString("schoolId", "") ?: "")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initialize() {
        viewModel.schoolData.observe(viewLifecycleOwner) { school ->
            with(_binding!!) {
                txtSchoolName.text = resources.getString(R.string.name, school.school_name)
                txtAddress.text =
                    resources.getString(R.string.address, school.primary_address_line_1)
                txtDescription.text =
                    resources.getString(R.string.details, school.overview_paragraph)
                txtReadSat.text = if (school.sat_critical_reading_avg_score?.isEmpty() == true) {
                    resources.getString(
                        R.string.reading,
                        resources.getString(R.string.not_available)
                    )
                } else {
                    resources.getString(R.string.reading, school.sat_critical_reading_avg_score)
                }
                txtWriteSat.text = if (school.sat_writing_avg_score?.isEmpty() == true) {
                    resources.getString(
                        R.string.writing,
                        resources.getString(R.string.not_available)
                    )
                } else {
                    resources.getString(R.string.writing, school.sat_writing_avg_score)
                }
                txtSat.text = if (school.sat_math_avg_score?.isEmpty() == true) {
                    resources.getString(R.string.math, resources.getString(R.string.not_available))
                } else {
                    resources.getString(R.string.math, school.sat_math_avg_score)
                }
                txtWebsite.text =
                    resources.getString(R.string.website,school.website)
            }
        }
    }
}