package com.onlinetest.schoolapp.ui

import android.graphics.Color
import android.view.View
import androidx.appcompat.app.AppCompatActivity

fun AppCompatActivity.setWindowInsets(){
    window?.apply {
        decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION

        var flags: Int = decorView.systemUiVisibility
        flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        decorView.systemUiVisibility = flags

        navigationBarColor = Color.WHITE
    }
}