package com.onlinetest.schoolapp.vm

import androidx.lifecycle.MutableLiveData
import com.onlinetest.core.BaseViewModel
import com.onlinetest.data.repository.SchoolAPIRepository
import com.onlinetest.model.domain.School
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class SchoolDetailViewModel @Inject constructor(
    private val schoolRepository: SchoolAPIRepository
): BaseViewModel()
{
    val schoolData = MutableLiveData<School>()

    fun fetchData(schoolId:String){
        compositeDisposable.add(
            schoolRepository.getSchool(schoolId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { school ->
                    schoolData.postValue(school)
                }
        )
    }
}