package com.onlinetest.schoolapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.onlinetest.model.domain.School

/**
 * class to generate the list of school UI
 */

//HACK- This class could have been implemented by using Paging library
// so that muc of he code could have been reduced and data could have been fetched directly from
//Db
class SchoolListAdapter(
    private val schoolsList: List<School>,
    private val itemClick: (String) -> Unit
) : RecyclerView.Adapter<SchoolListAdapter.SchoolViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_school_item, parent, false)
        return SchoolViewHolder(view)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        val school = schoolsList[position]
        holder.schoolNameText.text = school.school_name
        holder.itemView.setOnClickListener {
            itemClick.invoke(school.dbn)
        }
    }

    override fun getItemCount(): Int {
        return schoolsList.size
    }

    class SchoolViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val schoolNameText: AppCompatTextView = itemView.findViewById(R.id.txt_school_name)
    }

}

